const { Console } = require('console');
const mysql = require ('mysql2');

const connection = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: 'root',
    database: 'ceps'
});

connection.connect((error)=>{
    if(error)
    console.log(error);

    else
    console.log("CONECTADO");
});

module.exports = connection;