const express = require('express');
const db = require('./db/connection');
const {message} = require('statuses');
const port = 8000;

let app = express();
app.use(express.json())

//1 – Recurso para inclusão dos dados para controle de cep (ID, CEP, CIDADEUF, BAIRRO, LOCALIDADE, ATIVO). O Ativo terá valor 1

app.post('/controle', (req, res) => {
    let novoend = req.body;
    let cmd_insert = 'INSERT INTO CEP SET?'
    db.query(cmd_insert,novoend,(error,result) => {
        if(error) {
            res.status(400).json({message:"Informações inseridas é inválidas " + error}) 
        }else{
            res.status(201).json({message:"Novo endereço cadastro com sucesso " + result.insertId}) 
        }
    })
})

//4 - recurso de busca de cep, informar o cep via path. O retorno do recurso deverá ser um json com os atributos ID, CEP, CIDADEUF, BAIRRO e LOCALIDADE.

app.get('/controle/cep/:cep', (req, res) => {
    let buscacep = req.params.cep
    let busca = 'SELECT ID, CEP, CIDADEUF, BAIRRO, LOCALIDADE FROM CEP WHERE CEP=?'


    db.query(busca,buscacep,(err, rows)=>{
        res.status(200).json(rows);
    })
})

//5 – recurso de busca por bairro, informar o bairro via path. O retorno do recurso deverá ser um json com os atributos ID, CEP, CIDADEUF, BAIRRO e LOCALIDADE.

app.get('/controle/bairro/:bairro', (req, res) => {
    let buscacep = req.params.bairro
    let busca = 'SELECT ID, CEP, CIDADEUF, BAIRRO, LOCALIDADE FROM CEP WHERE BAIRRO=?'

    db.query(busca,buscacep,(err, rows)=>{
        res.status(200).json(rows);
    })
})

//6 – recurso de busca por cidade, informar a cidade via query. O retorno do recurso deverá ser um json com os atributos ID, CEP, CIDADEUF, BAIRRO e LOCALIDADE

app.get('/controle/cidade', (req, res) => {
    let buscacep = req.query.CIDADEUF
    let busca = 'SELECT ID, CEP, CIDADEUF, BAIRRO, LOCALIDADE FROM CEP WHERE CIDADEUF=?'

    db.query(busca,buscacep,(err, rows)=>{
        res.status(200).json(rows);
    })
})

//7 – recurso de busca por localidade, informar a cidade via query. O retorno do recurso deverá ser um json com os atributos ID, CEP, CIDADEUF, BAIRRO e LOCALIDADE.

app.get('/controle/localidade', (req, res) => {
    let buscacep = req.query.LOCALIDADE
    let busca = 'SELECT ID, CEP, CIDADEUF, BAIRRO, LOCALIDADE FROM CEP WHERE LOCALIDADE=?'

    db.query(busca,buscacep,(err, rows)=>{
        res.status(200).json(rows);
    })
})


//2 – recurso para alteração dos dados para controle de cep (CEP, CIDADEUR, BAIRRO, LOCALIDADE). O ID será passado via PARAM.

app.put('/controle/:id', (req, res) => {
    let id = req.params.id;
    let body = req.body;

    let sql = "UPDATE CEP SET CEP=?, CIDADEUF=?, BAIRRO=?, LOCALIDADE=? WHERE ID=?"
    let values = [body.CEP, body.CIDADEUF, body.BAIRRO, body.LOCALIDADE, id]
    
    db.query(sql, values,(error,result)=>{
        if (error) {
            res.status(400).json({message:"Informações inseridas é inválidas " + error})
        }else{
            res.status(201).json({message: "O ID " + id + " foi alterado com sucesso " })
        }
    })  
})

//3 – recurso para alteração dos dados para controle de cep. O id será passado via PATH (será realizado uma alterado do campo ATIVO para 0

app.put('/controle/ativo/:id', (req, res) => {
    let id = req.params.id;
    let body = req.body;

    let sql = "UPDATE CEP SET ATIVO=? WHERE ID=?"
    let values = [body.ATIVO, id]
    
    db.query(sql, values,(error,result)=>{
        if (error) {
            res.status(400).json({message:"Informações inseridas é inválidas " + error})
        }else{
            res.status(201).json({message: " Ativo " + id  + "  Alterado com sucesso " })
        }
    })  
})

//********************************************************************/




app.listen(port, () => {
    console.log("=============================")
    console.log("Rodando na porta " + port)
});